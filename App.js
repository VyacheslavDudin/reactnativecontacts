import React, { Component } from "react";
import {
  Platform,
  PermissionsAndroid,
  Button,
  TextInput,
  StyleSheet,
  View,
  TouchableOpacity
} from "react-native";
import Contacts, { deleteContact } from "react-native-contacts";

import { createSwitchNavigator, createAppContainer } from "react-navigation";
import { addContactPage } from "./containers/addContactPage";
import { contactInfo } from "./containers/contactInfo";
import { Contact } from "./components/Contact";

class App extends Component {
  constructor(props) {
    super(props);
    // It`s an example, it will be deleted after first getting contacts
    this.state = {
      contacts: [
        {
          givenName: "Gala",
          familyName: "Oo",
          recordID: "6b2237ee0df85980",
          phoneNumbers: [{ number: 111 }, { number: 121 }]
        },
        {
          givenName: "Vika",
          familyName: "Aa",
          recordID: "6b2237ee0df85982",
          phoneNumbers: [{ number: 333 }]
        }
      ],
      searchableText: ""
    };
    this.removeContact = this.removeContact.bind(this);
    this.onChangeText = this.onChangeText.bind(this);
  }
  componentDidMount() {
    if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "This app would like to view your contacts."
      }).then(() => {
        Contacts.getAll((err, contacts) => {
          if (err === "denied") {
            alert("Please, allow to use contacts");
          } else {
            this.setState({ contacts });
          }
        });
      });
    }
  }

  onChangeText(searchableText) {
    this.setState({ searchableText });
  }

  removeContact(contact) {
    deleteContact(contact);
    this.setState(({ contacts }) => ({
      contacts: contacts.filter(c => c.recordID !== contact.recordID)
    }));
  }

  render() {
    let {contacts, searchableText} = this.state;
    const filteredContacts = contacts.filter(
      c =>
        c.givenName.indexOf(searchableText) !== -1 ||
        c.familyName.indexOf(searchableText) !== -1
    );
    const mappedContacts = filteredContacts.map(c => (
      <TouchableOpacity
        key={c.recordID}
        style={styles.contact}
        onPress={() =>
          this.props.navigation.navigate("contactInfo", { contact: c })
        }
      >
        <Contact contact={c} removeContact={this.removeContact} />
      </TouchableOpacity>
    ));
    return (
      <View style={styles.app}>
        <View style={styles.header}>
          <TextInput
            style={styles.input}
            placeholder="Find a contact"
            editable
            onChangeText={text => this.onChangeText(text)}
          />
        </View>
        <View style={styles.contacts}>{mappedContacts}</View>
        <Button
          style={styles.addBtn}
          onPress={() => this.props.navigation.navigate("addContact")}
          title="Add contact"
        />
      </View>
    );
  }
}

const MySwitchNavigator = createSwitchNavigator(
  {
    app: App,
    addContact: addContactPage,
    contactInfo: contactInfo
  },
  {
    initialRouteName: "app"
  }
);

export const NavContainer = createAppContainer(MySwitchNavigator);

const styles = StyleSheet.create({
  app: {
    marginHorizontal: "auto",
    maxWidth: 500,
  },
  logo: {
    height: 80
  },
  header: {
    padding: 20
  },
  title: {
    fontWeight: "bold",
    fontSize: "1.5rem",
    marginVertical: "1em",
    textAlign: "center"
  },
  text: {
    lineHeight: "1.5em",
    fontSize: "1.125rem",
    marginVertical: "1em",
    textAlign: "center"
  },
  input: {
    borderWidth: 1,
    borderColor: "#000",
    height: 30,
    width: 280,
    padding: 8,
    borderRadius: 20
  }
});

export default App;
