import React from "react";
import Icon from "react-native-vector-icons/FontAwesome5";
import { View, Text, StyleSheet, Image } from "react-native";

export const Contact = props => {
  const { contact } = props;
  return (
    <View style={styles.contact}>
      {contact.hasThumbnail ? (
        <Image size={20} source={{ url: contact.thumbnailPath }} />
      ) : (
        <Icon
          name="snowman"
          size={20}
          color="#000"
          style={styles.delBtn}
          title="Contact icon"
        />
      )}
      <Text>
        {contact.givenName} {contact.familyName}
      </Text>
      <Icon
        name="trash-alt"
        size={30}
        color="#000"
        style={styles.delBtn}
        onPress={() => props.removeContact(contact)}
        title="Delete button"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  contacts: {
    flex: 1,
    justifyContent: "center",
    alignItems: "space-between",
    padding: 8
  },
  contact: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    height: 30,
    padding: 12,
    marginBottom: 6,
    backgroundColor: "pink",
    borderRadius: 10
  }
});
