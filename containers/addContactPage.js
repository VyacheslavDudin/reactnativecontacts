import React, { Component } from "react";
import { Text, View, TextInput, StyleSheet, Button } from "react-native";
import { addContact } from "react-native-contacts";

export class addContactPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contact: {
        tel: "",
        firstName: "",
        lastName: ""
      },
      isValidContact: false
    };
  }

  validateFields() {
    const isValidContact = Object.values(this.state.contact).every(
      v => v.trim() !== ""
    );
    this.setState({ isValidContact });
    return isValidContact;
  }

  onChangeFirstName(text) {
    this.setState(
      { contact: { ...this.state.contact, firstName: text } },
      this.validateFields
    );
    this.validateFields();
  }

  onChangeLastName(text) {
    this.setState(
      { contact: { ...this.state.contact, lastName: text } },
      this.validateFields
    );
  }

  onChangeTel(text) {
    const value = text.replace(/[^\d]/g, "");
    this.setState(
      { contact: { ...this.state.contact, tel: value } },
      this.validateFields
    );
  }

  onSubmitButton() {
    if (!this.validateFields()) {
      return;
    }

    const { firstName, lastName, tel } = this.state.contact;
    const newContact = {
      familyName: lastName,
      givenName: firstName,
      phoneNumbers: [
        {
          label: "mobile",
          number: tel
        }
      ]
    };
    addContact(newContact, err => {
      if (err) alert("Error! Contact was not created");
    });
    this.props.navigation.navigate("app");
  }
  render() {
    return (
      <View style={styles.addContactPage}>
        <View style={styles.header}>
          <Text style={styles.header}>Add a contact</Text>
        </View>
        <View style={styles.form}>
          <TextInput
            style={styles.input}
            placeholder="Tel*:"
            editable
            value={this.state.contact.tel}
            onChangeText={text => this.onChangeTel(text)}
          />
          <TextInput
            style={styles.input}
            placeholder="FirstName*:"
            editable
            value={this.state.contact.firstName}
            onChangeText={text => this.onChangeFirstName(text)}
          />
          <TextInput
            style={styles.input}
            placeholder="LastName*:"
            editable
            value={this.state.contact.lastName}
            onChangeText={text => this.onChangeLastName(text)}
          />
          <View style={styles.btns}>
            <Button
              onPress={() => this.props.navigation.navigate("app")}
              title="Back"
            />
            <Button
              onPress={() => this.onSubmitButton()}
              title="Add contact"
              disabled={!this.state.isValidContact}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  header: {
    fontSize: 28,
    fontWeight: "bold",
    textAlign: "center",
    margin: 15
  },
  input: {
    borderWidth: 1,
    borderColor: "#000",
    height: 30,
    width: 280,
    padding: 8,
    margin: 5
  },
  form: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  btns: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  }
});
