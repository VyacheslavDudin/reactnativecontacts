import React from "react";
import { Text, View, StyleSheet, Linking, Image, Button } from "react-native";
import Icon from "react-native-vector-icons/Feather";

export const contactInfo = props => {
  const { contact: contactInfo } = props.navigation.state.params;
  return (
    <View style={styles.infoPage}>
      <View style={styles.header}>
        <Text style={styles.header}>Information about contact</Text>
      </View>
      <View style={styles.info}>
        {contactInfo.hasThumbnail ? (
          <Image size={40} source={{ url: contactInfo.thumbnailPath }} />
        ) : (
          <Icon name="snowman" size={40} color="#000" title="Contact icon" />
        )}
      </View>
      <View style={styles.contactInfo}>
        <Text style={styles.infoText}>FirstName: {contactInfo.givenName}</Text>
        <Text style={styles.infoText}>LastName: {contactInfo.familyName}</Text>
        <Text style={styles.infoText}>
          PhoneNumber: {contactInfo.phoneNumbers[0].number}
        </Text>
      </View>
      <View style={styles.btns}>
        <Button onPress={() => props.navigation.navigate("app")} title="Back" />
        <Icon
          name="phone-call"
          size={30}
          color="#000"
          onPress={() =>
            Linking.openURL(`tel:${contactInfo.phoneNumbers[0].number}`)
          }
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    fontSize: 28,
    fontWeight: "bold",
    textAlign: "center",
    margin: 15
  },
  input: {
    borderWidth: 1,
    borderColor: "#000",
    height: 30,
    width: 280,
    padding: 8,
    margin: 5
  },
  form: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  info: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  contactInfo: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20
  },
  infoText: {
    fontSize: 18,
    fontWeight: "bold"
  },
  btns: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-evenly"
  }
});
