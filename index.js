/**
 * @format
 */

import { AppRegistry } from "react-native";
import { NavContainer } from "./App";

AppRegistry.registerComponent('App', () => NavContainer);
